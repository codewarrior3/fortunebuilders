﻿using System;

namespace MortgageClassLibrary
{
    public class MortgageCalculator
    {
        public double Addition(double x, double y)
        {
            return x + y;
        }

        public double Subtraction(double x, double y)
        {
            return x - y;
        }

        public double Multiplication(double x, double y)
        {
            return x * y;
        }

        public double Division(double x, double y)
        {
            return x / y;
        }

        /*The total worth of the loan is compounded monthly, and the monthly payment is determined by dividing the entire compounded cost of the loan by the total number of months.*/

        public double CalculateTheMortgage(double loanAmount, double taxesPerYear, double downPayment, double interestRate, double termOfLoan, double propertyTax, double insurance)
        {
            //  (Loan Value) *  (1 + r/12) ^ p =  (12x / r)  *  ((1 + r/12)^p - 1)
            // payment = (((Loan Value) *  (1 + r/12) ^ p) * r)/ (12 * ((1 + r/12)^p - 1)));

            double payment = (loanAmount - downPayment) * (Math.Pow((1 + interestRate / 12), termOfLoan) * interestRate) / (12 * (Math.Pow((1 + interestRate / 12), termOfLoan) - 1));

            // add on a monthly property tax
            payment = payment + (propertyTax + insurance) / 12;

            return payment;
        }
    }
}