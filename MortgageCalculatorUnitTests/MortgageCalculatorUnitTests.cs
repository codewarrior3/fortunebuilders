﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MortgageClassLibrary;

namespace MortgageCalculatorUnitTests
{
    [TestClass]
    public class MortgageCalculatorUnitTests
    {
        [TestMethod]
        public void TestAddition()
        {
            // Tests the Mortgage Calculator Addition Method
            double currentBalance = 10.0;
            double amountToAdd = 15.0;
            const double expected = 25.0;
            MortgageCalculator mortgageCalculator = new MortgageCalculator();
            double result = mortgageCalculator.Addition(currentBalance, amountToAdd);
            // assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestSubtraction()
        {
            // Tests the Mortgage Calculator Subtraction Method
            double currentBalance = 30.0;
            double amountToSubtract = 11.0;
            const double expected = 19.0;
            MortgageCalculator mortgageCalculator = new MortgageCalculator();
            double result = mortgageCalculator.Subtraction(currentBalance, amountToSubtract);
            // assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestMultiplication()
        {
            // Tests the Mortgage Calculator Multiplication Method
            double currentBalance = 30.0;
            double amountToMultiply = 3.0;
            const double expected = 90.0;
            MortgageCalculator mortgageCalculator = new MortgageCalculator();
            double result = mortgageCalculator.Multiplication(currentBalance, amountToMultiply);
            // assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestDivision()
        {
            // Tests the Mortgage Calculator Division Method
            double currentBalance = 30.0;
            double amountToDivide = 10.0;
            const double expected = 3.0;
            MortgageCalculator mortgageCalculator = new MortgageCalculator();
            double result = mortgageCalculator.Division(currentBalance, amountToDivide);
            // assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void TestCalculateMortgage()
        {
            double loanAmount = 120000.00; // price of total mortgage before down payment
            double taxesPerYear = 0.0; // this will divided by 12 and added to the monthly payment
            double downPayment = 20000.00; // down payment will be subtracted from the loan
            double interestRate = .06;//(6 / 100);  // calculate interest from 100%
            double termOfLoan = (30 * 12); // monthly term
            double propertyTax = 0.0;
            double insurance = 0.0;

            const int expected = 599;

            MortgageCalculator mortgageCalculator = new MortgageCalculator();

            int MonthlyPaymentResult = (int)mortgageCalculator.CalculateTheMortgage(loanAmount, taxesPerYear, downPayment,
                interestRate, termOfLoan, propertyTax, insurance);

            Assert.AreEqual(expected, MonthlyPaymentResult);
        }
    }
}