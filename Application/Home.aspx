﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Application.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Home</h1>

    <h2>Mortgage Calculator</h2>
    <table>
        <tr>
            <td>
                <asp:Label ID="lblPurchasePrice" runat="server" Text="Purchase Price"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtPurchasePrice" MaxLength="10" Text="120000" runat="server"></asp:TextBox><asp:Label ID="Label5" runat="server" Text="Whole Numbers Only"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblLoanTerm" runat="server" Text="Loan Term:"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtLoanTerm" MaxLength="2" Text="30" runat="server"></asp:TextBox><asp:Label ID="Label4" runat="server" Text="(Years). Whole Numbers Only: 0-99"></asp:Label>
            </td>
        </tr>

        <tr>
            <td>
                <asp:Label ID="lblInterest" runat="server" Text="Interest:"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtInterest" Text="6" MaxLength="2" runat="server"></asp:TextBox><asp:Label ID="Label3" runat="server" Text="Whole Numbers Only: Enter 6 for %6 "></asp:Label>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                    ControlToValidate="txtInterest" runat="server"
                    ErrorMessage="Only Numbers allowed"
                    ValidationExpression="\d+">
                </asp:RegularExpressionValidator></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblDownPayment" runat="server" Text="Down Payment:"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtDownPayment" Text="20000" MaxLength="6" runat="server"></asp:TextBox><asp:Label ID="Label6" runat="server" Text="Whole Numbers Only"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblPropertyTax" runat="server" Text="Property Tax:"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtPropertyTax" MaxLength="5" Text="0" runat="server"></asp:TextBox><asp:Label ID="Label2" runat="server" Text="Per Year. "></asp:Label><asp:Label ID="Label7" runat="server" Text=" Whole Numbers Only"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="PMIInsurance" runat="server" Text="PMI (Insurance):"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtPMIInsurance" MaxLength="5" Text="0" runat="server"></asp:TextBox><asp:Label ID="Label1" runat="server" Text="Per Year. "></asp:Label><asp:Label ID="Label8" runat="server" Text=" Whole Numbers Only"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblMonthlyPayment" runat="server" Text="Monthly Payment:"></asp:Label></td>
            <td>
                <asp:Label ID="lblMonthlyPaymentResult" runat="server"></asp:Label></td>
        </tr>
    </table>
    <asp:Button ID="btnCalculate" runat="server" OnClick="btnCalculate_OnClick" Text="Calculate" />
    <asp:Label ID="lblError" Visible="False" runat="server" Text="Error" ForeColor="red"></asp:Label>
</asp:Content>