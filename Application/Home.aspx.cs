﻿using MortgageClassLibrary;
using System;

namespace Application
{
    public partial class Home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnCalculate_OnClick(object sender, EventArgs e)
        {
            MortgageCalculator mortgageCalculator = new MortgageCalculator();

            bool parseLoanAmountSuccess = false;
            bool parseTaxesPerYearSuccess = false;
            bool parseDownPaymentSuccess = false;
            bool parseInterestRateSuccess = false;
            bool parseTermOfLoanSuccess = false;
            bool parsePropertyTaxSuccess = false;
            bool parseInsuranceSuccess = false;

            double loanAmount; //whole numbers only price of total mortgage before down payment
            double taxesPerYear; // this will divided by 12 and added to the monthly payment
            double downPayment; // down payment will be subtracted from the loan
            double interestRate; //(6 / 100)=.06;  // calculate interest from 100%
            double termOfLoan;//(30 * 12); // monthly term
            double propertyTax;//(0.0);
            double insurance;//0.0;

            parseLoanAmountSuccess = double.TryParse(txtPurchasePrice.Text.Trim(), out loanAmount);
            parseTaxesPerYearSuccess = double.TryParse(txtPropertyTax.Text.Trim(), out taxesPerYear);
            parsePropertyTaxSuccess = double.TryParse(txtPropertyTax.Text.Trim(), out propertyTax);
            parseDownPaymentSuccess = double.TryParse(txtDownPayment.Text.ToString(), out downPayment);
            parseInterestRateSuccess = double.TryParse(txtInterest.Text.Trim(), out interestRate);
            interestRate = interestRate / 100;
            parseInsuranceSuccess = double.TryParse(txtPMIInsurance.Text.Trim(), out insurance);
            parseTermOfLoanSuccess = double.TryParse(txtLoanTerm.Text.Trim(), out termOfLoan);
            termOfLoan = termOfLoan * 12;

            if (parseLoanAmountSuccess == false || parseTaxesPerYearSuccess == false ||
                parsePropertyTaxSuccess == false || parseDownPaymentSuccess == false ||
                parseInterestRateSuccess == false || parseInsuranceSuccess == false ||
                parseTermOfLoanSuccess == false)
            {
                lblError.Visible = true;
            }
            else
            {
                lblError.Visible = false;
            }

            int MonthlyPaymentResult = (int)mortgageCalculator.CalculateTheMortgage(loanAmount, taxesPerYear, downPayment,
                interestRate, termOfLoan, propertyTax, insurance);

            lblMonthlyPaymentResult.Text = MonthlyPaymentResult.ToString().Trim();
        }
    }
}